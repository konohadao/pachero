﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameTool;

public class LeaderBoardSystem : SingletonMonoBehaviour<LeaderBoardSystem>
{
    public List<GameObject> players = new List<GameObject>();
    GameObject player;
    [SerializeField]
    GameObject playerDisplay;
    public int t;


    [SerializeField] Text playerScore;
    [SerializeField] Text playerName;
    [SerializeField] Text playerTop;
    [SerializeField] Image playerAvt;

    private void OnEnable()
    {   
        this.RegisterListener(EventID.PlayingGame,(sender,param)=>GetListAI());
        this.RegisterListener(EventID.CollectExp,(sender,param)=>GetListAI());
    }
    private void OnDisable()
    {
   
        this.RemoveListener(EventID.PlayingGame, (sender, param) => GetListAI());
        this.RemoveListener(EventID.CollectExp, (sender, param) => GetListAI());
    }
    public void GetListAI()
    {
        players = IOGameController.Instance.playersList;
        if(IOGameController.Instance.isPlaying)
        StartCoroutine(WaitFor());
    }
    void GetPlayer()
    {
        foreach (GameObject g in players)
        {
            if (g.gameObject.name == "Player")
                player = g;
        }
    }
    IEnumerator WaitFor()
    {
        yield return new WaitForSeconds(0.1f);      
        GetLeaderBoardList();
        GetPlayer();
    }
   
  
    public void GetLeaderBoardList()
    {
        if (players != null)
        {
            for (int i = 0; i < players.Count; i++)
            {
                for (int j = i + 1; j < players.Count; j++)
                {
                    if (players[i] != null && players[j] != null)
                    {
                        if (players[j].GetComponent<GetPro>().GetScalePlayer() > players[i].GetComponent<GetPro>().GetScalePlayer())
                        {
                            GameObject tmp = players[i];
                            players[i] = players[j];
                            players[j] = tmp;
                        }
                    }
                }
            }
        }
        if (player != null)
        {
            playerScore.text = player.GetComponent<GetPro>().GetScalePlayer().ToString();
            playerName.text = player.GetComponent<GetPro>().GetNamePlayer().ToString();
            playerTop.text = GetPlayerTop().ToString();
           /// playerAvt.sprite = player.GetComponent<GetPro>().GetAvtPlayer();
            //if (players[2] != null)
            //{
            //    if (player.GetComponent<GetPro>().GetScorePlayer() <= players[2].GetComponent<GetPro>().GetScorePlayer())
            //    {
            //        if (player.GetComponent<GetPro>().GetNamePlayer() != players[2].GetComponent<GetPro>().GetNamePlayer())
            //            playerDisplay.SetActive(true);
            //        else
            //            playerDisplay.SetActive(false);
            //    }
            //    else
            //        playerDisplay.SetActive(false);
            //}

            //if (GetPlayerTop() > 3)
            //{
            //    playerDisplay.SetActive(true);
            //}
            //else
            //    playerDisplay.SetActive(false);
        }
    }
    public int GetPlayerTop()
    {
        if (player != null && players!=null)
        {
            for (int i = 0; i < players.Count; i++)
            {
                //if (player.GetComponent<GetPro>().GetScorePlayer() == players[i].GetComponent<GetPro>().GetScorePlayer())
                //{
                //    t = i;
                   
                //}
                if(players[i].gameObject.name == "Player")
                {
                    t = i;
                }
            }
        }
        return t + 1;
    }
}
   
