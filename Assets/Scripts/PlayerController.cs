﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameTool;

public class PlayerController : PacmanBase
{

    public Joystick joystick;
    private void OnEnable()
    {
         
        this.name = PlayerPrefs.GetString("playerName");
        textname.GetComponent<Text>().text = PlayerPrefs.GetString("playerName");
        this.RegisterListener(EventID.StartGame, (sender, param) => NameUp());
    }
    void NameUp()
    {
        this.name = IOGameController.Instance.GetNamePlayerFromInput();
        textname.GetComponent<Text>().text = IOGameController.Instance.GetNamePlayerFromInput();
    }

    //private void Start()
    //{
    //    this.SetName(IOGameController.Instance.GetNamePlayerFromInput());
    //}
    private void FixedUpdate()
    {
        Vector3 moveVector = (Vector3.right * joystick.Horizontal + Vector3.up * joystick.Vertical);

        if (moveVector != Vector3.zero)
        { 
            transform.rotation = Quaternion.LookRotation(new Vector3(0, 0, 1), moveVector);
            transform.Translate(moveVector * moveSpeed * Time.deltaTime, Space.World);
            // StartCoroutine(effCreator(0.05f));
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Exp")
        {
            this.TakeExp(GameConfig.expvalue);
           
            this.PostEvent(EventID.CollectExp);
            collision.gameObject.transform.position = GetRandomPositon();
        }
    }
   
    Vector2 GetRandomPositon()
    {
        float ranX = Random.Range(GameConfig.minX + 1, GameConfig.maxX - 1);
        float ranY = Random.Range(GameConfig.minY + 1, GameConfig.maxY - 1);
        Vector2 postion = new Vector2(ranX, ranY);
        return postion;
    }
}
