﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorControll : SingletonMonoBehaviour<ColorControll>
{
    public List<Color> colors = new List<Color>();
    private List<Color> colorsCopy;
    private void Start()
    {
        colorsCopy = colors;
    }
    public Color GetColor()
    {
        int i = Random.Range(0, colors.Count);
        Color c = colors[i];
        colors.Remove(colors[i]);
        return c;
    }
    public Color GetColorExp()
    {
        int i = Random.Range(0, colorsCopy.Count);
        Color c = colorsCopy[i];
        return c;
    }
}
