﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetPro : MonoBehaviour
{
    [SerializeField] GameObject player;

    public float GetScalePlayer()
    {
        float scale;
        if (player.gameObject.name == "Player")
        {
            scale = player.GetComponent<PlayerController>().GetLocalScale();
        }
        else
            scale = player.GetComponent<Bot>().GetLocalScale();

        return scale;
    }
    public string GetNamePlayer()
    {
        string name;
        if (player.gameObject.name == "Player")
        {
            name = player.GetComponent<PlayerController>().GetName();
        }
        else
            name = player.GetComponent<Bot>().GetName();

        return name;
    }
    //public Sprite GetAvtPlayer()
    //{
    //    Sprite avt;
    //    if (player.gameObject.name == "Player")
    //    {
    //        avt = player.GetComponent<PlayerController>().GetAvt();
    //    }
    //    else
    //        avt = player.GetComponent<AIController>().GetAvt();

    //    return avt;
    //}

    //public bool GetStatus()
    //{
    //    bool status;
    //    if (player.gameObject.name == "Player")
    //    {
    //        status = player.GetComponent<PlayerController>().GetStatus();
    //    }
    //    else
    //        status = player.GetComponent<AIController>().GetStatus();

    //    return status;
    //}

}
