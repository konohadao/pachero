﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameTool;

public class Bot : PacmanBase
{
   // float[] ranPlusScale = { 0.05f, 0.1f,0.01f,0.02f,0.03f,0.04f,0.06f,0.07f,0.08f };
    Vector2 targetMove;
    int min;
    void Start()
    {
        //Vector3 oldSize = this.transform.localScale;
        //float t = ranPlusScale[Random.Range(0, ranPlusScale.Length)];
        //this.transform.position = new Vector3(oldSize.x + t, +oldSize.y + t, oldSize.z + t);
        this.SetName(IOGameController.Instance.RandomName());
        textname.GetComponent<Text>().text = this.name;
        targetMove = GetRandomPositon();
        this.gameObject.GetComponent<SpriteRenderer>().color = ColorControll.Instance.GetColor();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Vector2.Distance(this.transform.position, targetMove) <= 1)
        {
            targetMove = GetRandomPositon();
        }
        else
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, targetMove, moveSpeed * Time.deltaTime);
            transform.rotation = Quaternion.LookRotation(new Vector3(0, 0, 1), targetMove);
          //  transform.LookAt(targetMove);
        }
       // MoveToExp();

    }

    //void MoveToExp()
    //{
       
    //    if(IOGameController.Instance.expList[0]!= null)
    //    {
    //        for(int i=0;i< IOGameController.Instance.expList.Count;i++)
    //        {
    //            for(int j = i+1;j< IOGameController.Instance.expList.Count;j++)
    //            {
    //                if (Vector2.Distance(this.gameObject.transform.position, IOGameController.Instance.expList[i].transform.position) < Vector2.Distance(this.gameObject.transform.position, IOGameController.Instance.expList[j].transform.position))
    //                {
    //                    min = i;
    //                }
    //                else
    //                    min = j;
    //            }
    //        }
    //        targetMove = IOGameController.Instance.expList[min].transform.position;
    //    }

    //}
    Vector2 GetRandomPositon()
    {       
        float ranX = Random.Range(GameConfig.minX, GameConfig.maxX);
        float ranY = Random.Range(GameConfig.minY, GameConfig.maxY);
        Vector2 postion = new Vector2(ranX, ranY);
        return postion;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Exp")
        {
           
            this.PostEvent(EventID.CollectExp);
            this.TakeExp(GameConfig.expPlayervalue);
            collision.gameObject.transform.position = GetRandomPositon();
        }
    }
    
}
