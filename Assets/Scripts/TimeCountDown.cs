﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCountDown : MonoBehaviour
{
    float totalTime = GameConfig.TimePlay;
    public Text timeText;
    private void Update()
    {
        if (IOGameController.Instance.isPlaying)
        {
            totalTime -= Time.deltaTime;
            UpdateLevelTimer(totalTime);
        }
        else
        {
            timeText.text = "00:00";
        }
        if(totalTime<=0)
        {
            totalTime = 0;
            IOGameController.Instance.GameOver();
        }
    }

    public void UpdateLevelTimer(float totalSeconds)
    {
        int minutes = Mathf.FloorToInt(totalSeconds / 60f);
        int seconds = Mathf.RoundToInt(totalSeconds % 60f);

        string formatedSeconds = seconds.ToString();

        if (seconds == 60)
        {
            seconds = 0;
            minutes += 1;
        }

        timeText.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }
}
