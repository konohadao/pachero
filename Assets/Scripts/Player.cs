﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public Color[] color;
    SpriteRenderer sprite;
    public float moveSpeed;
    public int playerValue;
    public  Rigidbody2D rig;
    public Joystick joystick;
    public TrailRenderer trail;
   // public GameObject eff;

  //  Vector2 posEff;
    // Use this for initialization

    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        trail = GetComponent<TrailRenderer>();

        setColor();
        StartCoroutine(nextColor(10f));
    }
    void setColor()
    {
        playerValue = Random.Range(0, 3);
        sprite.color = color[playerValue];

    }
    // Update is called once per frame
    void Update()
    {
      //  posEff = new Vector2(this.transform.position.x + Random.Range(0f, -0.5f), this.transform.position.y + Random.Range(-0.5f, 0.5f));

        trail.startColor = sprite.color;
        trail.startWidth = this.transform.localScale.y * 3f;
        

        Vector3 moveVector = (Vector3.right * joystick.Horizontal + Vector3.up * joystick.Vertical);

        if (moveVector != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(new Vector3(0,0,1), moveVector);
            transform.Translate(moveVector * moveSpeed * Time.deltaTime, Space.World);
           // StartCoroutine(effCreator(0.05f));
        }
        
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector2.left * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector2.right * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector2.up * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector2.down * moveSpeed * Time.deltaTime);
        } 
    }

     void OnTriggerEnter2D(Collider2D collision)
    {
        
            
    }

    IEnumerator nextColor(float time)
    {
        yield return new WaitForSeconds(time);
        setColor();
        StartCoroutine (nextColor(time));
    }
  /*  IEnumerator effCreator(float time)
    {
        yield return new WaitForSeconds(time);
        Instantiate(eff,posEff, Quaternion.identity);
        StartCoroutine(effCreator(time));
    } */
   
}

