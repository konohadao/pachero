﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
public class Achievement
{  
    public string ID;
    
    public string Name;
   
    public string Description;
  
    public bool isUnlock;

    public int progress
    {
        set { this.progress = value; }
        get { return this.progress; }
    }
    public void LoadData()
    {
        if (PlayerPrefs.HasKey(this.ID + "progress"))
        {
            this.progress = PlayerPrefs.GetInt(this.ID + "progress", 0);
        }
        else
         PlayerPrefs.SetInt(this.ID + "unlock", 0);
        if (!PlayerPrefs.HasKey(this.ID + "unlock"))
        {
             PlayerPrefs.GetInt(this.ID + "unlock", 0);
        }
    }
    public int targetProgress;

    public void Unlock()
    {
            PlayerPrefs.SetInt(this.ID + "unlock", 1);
            this.isUnlock = true;
       
    }
    public void Lock()
    {
        PlayerPrefs.SetInt(this.ID + "unlock", 0);
        this.isUnlock = false;
    }
    public void AddProgress(int value)
    {
        this.progress += value;
        PlayerPrefs.GetInt(this.ID + "progress", progress);
    }

}
