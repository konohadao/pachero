﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameTool;

public class LeaderBoardItem : MonoBehaviour
{
    [SerializeField] Text scoreText;
    [SerializeField] Text nameText;
    [SerializeField] Image imgAvt;
    [SerializeField] GameObject deathIcon;
    [SerializeField] int top;
    [SerializeField] int TYPE;
    // Start is called before the first frame update
    private void OnEnable()
    {      
            this.RegisterListener(EventID.CollectExp, (sender, param) => GetTopProp());
            this.RegisterListener(EventID.PlayingGame, (sender, param) => GetTopProp());
       
    }
    private void Start()
    {
        if(TYPE==1)
        {            
            Get();
        }
    }
    private void OnDisable()
    {
        this.RemoveListener(EventID.CollectExp, (sender, param) => GetTopProp());
        this.RemoveListener(EventID.PlayingGame, (sender, param) => GetTopProp());
    }
    
    void GetTopProp()
    {
        StartCoroutine(WaitFor());            
    }
    IEnumerator WaitFor()
    {    
        yield return new WaitForSeconds(0.15f);
        if (LeaderBoardSystem.Instance.players[top] != null)
        {
            scoreText.text = LeaderBoardSystem.Instance.players[top].GetComponent<GetPro>().GetScalePlayer().ToString();
            nameText.text = LeaderBoardSystem.Instance.players[top].GetComponent<GetPro>().GetNamePlayer().ToString();
          //  imgAvt.sprite = LeaderBoardSystem.Instance.players[top].GetComponent<GetPro>().GetAvtPlayer();

            //if (this.nameText.text == GameData.GetPlayerName())
            //{
            //    // Debug.Log(this.nameText.text + ":" + GameData.GetPlayerName() + ":" + LeaderBoardSystem.Instance.players[top].GetComponent<GetPro>().GetNamePlayer());
            //    //this.scoreText.color = new Color32(225, 225, 0, 225);
            //    //this.nameText.color = new Color32(225, 225, 0, 225);
            //}
        }
    }
    
    private void Get()
    {
        if (LeaderBoardSystem.Instance.players[top] != null)
        {
           
            //    scoreText.text = LeaderBoardSystem.Instance.players[top].GetComponent<GetPro>().GetScorePlayer().ToString();
            //    nameText.text = LeaderBoardSystem.Instance.players[top].GetComponent<GetPro>().GetNamePlayer().ToString();
        
            //deathIcon.SetActive(LeaderBoardSystem.Instance.players[top].GetComponent<GetPro>().GetStatus());
           // imgAvt.sprite = LeaderBoardSystem.Instance.players[top].GetComponent<GetPro>().GetAvtPlayer();
        }
    }

}
    
