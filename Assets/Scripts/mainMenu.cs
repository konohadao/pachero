﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class mainMenu : MonoBehaviour {

    public Button playBtb, quitBtn;
    public GameObject Play, title, target, target2;

    private void Start()
    {
        
        Play.transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.5f).SetLoops(-1, LoopType.Yoyo);
        title.transform.DOMove(target.transform.position, 0.8f);
    }
    public void playScore()
    {
        
        title.transform.DOMove(target2.transform.position, 0.5f).OnComplete(() =>
        {
            SceneManager.LoadScene("ScoreMode");
        });
        AdsManager.Instance.showInter();
    }
    public void playEndless()
    {
        AdsManager.Instance.showInter();
        title.transform.DOMove(target2.transform.position, 0.5f).OnComplete(() =>
        {
            SceneManager.LoadScene("EndlessMode");
        });
     
    }
    public void quitGame()
    {
        Application.Quit();
    }
}
