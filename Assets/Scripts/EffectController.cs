﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EffectController : SingletonMonoBehaviour<EffectController>
{
    [SerializeField] GameObject pacTxt;
    [SerializeField] GameObject ioTxt;
    [SerializeField] GameObject PlayBtn;
    [SerializeField] GameObject TextBox;
    [SerializeField] GameObject pos1pac, pos2pac, pos1io, pos2io;
    [SerializeField] GameObject pos1play, pos2play, pos1tb, pos2tb;
    [SerializeField] GameObject LeaderBoard,pos1Leader,pos2Leader;
    // Start is called before the first frame update
    public void GoMenu()
    {
        pacTxt.transform.position = pos2pac.transform.position;
        ioTxt.transform.position = pos2io.transform.position;
        pacTxt.transform.DOMove(pos1pac.transform.position, GameConfig.timeToEffect);
        ioTxt.transform.DOMove(pos1io.transform.position, GameConfig.timeToEffect);


        PlayBtn.transform.position = pos2play.transform.position;
        TextBox.transform.position = pos2tb.transform.position;
        PlayBtn.transform.DOMove(pos1play.transform.position, GameConfig.timeToEffect);
        TextBox.transform.DOMove(pos1tb.transform.position, GameConfig.timeToEffect);


    }
    public void OutMenu()
    {
        pacTxt.transform.DOMove(pos2pac.transform.position, GameConfig.timeToEffect);
        ioTxt.transform.DOMove(pos2io.transform.position, GameConfig.timeToEffect);

        PlayBtn.transform.DOMove(pos2play.transform.position, GameConfig.timeToEffect);
        TextBox.transform.DOMove(pos2tb.transform.position, GameConfig.timeToEffect);
    }
    public void PlayGame()
    {
        LeaderBoard.transform.DOMove(pos1Leader.transform.position, 0.5f);
    }
}
