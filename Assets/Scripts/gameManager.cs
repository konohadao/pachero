﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour {

    public static int score, heartUI,highscore;
    public GameObject gameOverPanel;

    
    public Text scoretxt,hearttxt,highscoretxt;
	// Use this for initialization
	void Start () {    
        highscoretxt.text = "High Score: " + PlayerPrefs.GetInt("High Score", 0).ToString();
        gameOverPanel.SetActive(false);
        heartUI = 5;
        score = 0;
    }
	
	// Update is called once per frame
	void Update () {

        if (score > PlayerPrefs.GetInt("High Score") )
        {
            PlayerPrefs.SetInt("High Score", score);
            highscoretxt.text = "High Score: " + score.ToString();
        }

        scoretxt.text = "Score: "+ score.ToString();
        hearttxt.text = " x " + heartUI.ToString();
        if (heartUI <= 0)
        {
            gameOver();
            
        }
            
    }
    public void rePlay()
    {
        SceneManager.LoadScene("ScoreMode");
        AdsManager.Instance.showInter();
    }
    public void mainMenu()
    {
        AdsManager.Instance.showInter();
        SceneManager.LoadScene("MainMenu");
    }
    public void gameOver()
    {      
        gameOverPanel.SetActive(true);
       
    }
}
