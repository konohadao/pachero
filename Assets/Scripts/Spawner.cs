﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameTool;

public class Spawner : SingletonMonoBehaviour<Spawner> {
    public GameObject ExpPrefab;
    public GameObject BotPrefab;
    public GameObject CanvasParent;
    public int BotCount;
    public int ExpCount;
    
    Vector2 GetRandomPositon()
    {
        float ranX = Random.Range(GameConfig.minX+1, GameConfig.maxX-1);
        float ranY = Random.Range(GameConfig.minY+1, GameConfig.maxY-1);
        Vector2 postion = new Vector2(ranX, ranY);
        return postion;
    }
    private void OnEnable()
    {
        this.RegisterListener(EventID.StartGame, (sender, param) => CreateBot());
    }
    private void OnDisable()
    {
        this.RemoveListener(EventID.StartGame, (sender, param) => CreateBot());
    }
    void CreateBot()
    {
        for (int i = 0; i < BotCount; i++)
        {
            GameObject bot = Instantiate(BotPrefab, GetRandomPositon(), Quaternion.identity);
            bot.transform.SetParent(CanvasParent.transform);
        }
        CreateExp();
        
    }
    void CreateExp()
    {
        for (int i = 0; i < ExpCount; i++)
        {
            GameObject exp = Instantiate(ExpPrefab, GetRandomPositon(), Quaternion.identity);
            exp.transform.SetParent(this.transform);
           // IOGameController.Instance.expList.Add(exp);
        }
        this.PostEvent(EventID.PlayingGame);
    }
    //public void CreateExpOnDead(Vector3 positon, int level)
    //{
    //    for (int i = 0; i < level; i++)
    //    {
    //        float t = Random.Range(-GameConfig.randomValueSpwanAfterDead, GameConfig.randomValueSpwanAfterDead);
    //        Vector3 spawnPostion = new Vector3(positon.x + t, positon.y + t, positon.z + t);
            
    //        GameObject exp = Instantiate(ExpPrefab, spawnPostion, Quaternion.identity);
    //        exp.transform.SetParent(this.transform);
    //        //IOGameController.Instance.expList.Add(exp);
    //    }
    //}

}
