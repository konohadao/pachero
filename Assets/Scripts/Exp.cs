﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exp : MonoBehaviour
{
    private void Start()
    {
        this.GetComponent<SpriteRenderer>().color = ColorControll.Instance.GetColorExp();
        float ran = Random.Range(0.1f, 0.3f);
        this.gameObject.GetComponent<Transform>().localScale = new Vector3(ran, ran, ran);
    }
}
