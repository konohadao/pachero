﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScene : MonoBehaviour
{
    public Slider slider;
    public int sceneIndex;
    public int timeToWait;
    void Start()
    {
        StartCoroutine(LoadAsynchoronously(sceneIndex));
    }

    IEnumerator LoadAsynchoronously(int sceneIndex)
    {
        slider.value = 0.2f;
        yield return new WaitForSeconds(timeToWait);        
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        while(!operation.isDone)
        {
            float progress = operation.progress;
            slider.value = progress;
            Debug.Log(progress);
            yield return null;
        }
    }
}
