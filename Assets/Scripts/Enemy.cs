﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    public float enemySpeed;
    public Transform heart;
    public Rigidbody2D rig;
    public Color[] color;
    SpriteRenderer sprite;
    public static int enemyValue;

    // Use this for initialization
    void Start () {
        heart = FindObjectOfType<Player>().GetComponent<Transform>();
        rig = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        setballColor();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(this.transform.position, heart.transform.position, enemySpeed * Time.deltaTime);
    }

    void setballColor()
    {
        enemyValue = Random.Range(0, 3);
        sprite.color = color[enemyValue];
        gameObject.tag = "enemy" + enemyValue;
    }
}
