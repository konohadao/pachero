﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameTool;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IOGameController : SingletonMonoBehaviour<IOGameController>
{
    [SerializeField] GameObject GameUI;

    [SerializeField] GameObject GameOverPanel;
    // [SerializeField] GameObject TimeOutPanel;
    public bool isPlaying;
    [SerializeField] GameObject Player;
    [SerializeField] GameObject Board;
    [SerializeField] InputField playerName;
    public List<GameObject> playersList = new List<GameObject>();
  //  public List<GameObject> expList = new List<GameObject>();

    public List<string> listName = new List<string>();

    public GameObject timeText;
    // Start is called before the first frame update
    void Start()
    {
        AdsManager.Instance.showInter();
        isPlaying = true;
        EffectController.Instance.GoMenu();
        timeText.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        GameOverPanel.SetActive(false);     
        playerName.text = PlayerPrefs.GetString("playerName");
        this.RegisterListener(EventID.PlayingGame, (sender, param) => GetListPlayer());
        GetNameFromResource();
       
    }
    public void StartGame()
    {
        AdsManager.Instance.showInter();
        PlayerPrefs.SetString("playerName",playerName.text);
        Player.SetActive(true);
        this.PostEvent(EventID.StartGame);
        EffectController.Instance.OutMenu();
        EffectController.Instance.PlayGame();
        GameUI.SetActive(true);
        timeText.SetActive(true);
       
    }
    public string GetNamePlayerFromInput()
    {
        if (!PlayerPrefs.HasKey("playerName"))
            return playerName.text;
        else
            return PlayerPrefs.GetString("playerName");
    }
    public void GetListPlayer()
    {
        GameObject[] g = GameObject.FindGameObjectsWithTag("Player");
        foreach(GameObject t in g)
        {
            playersList.Add(t);
        }
    }

    void GetNameFromResource()
    {       
        string str = Resources.Load<TextAsset>("Name").ToString();
        string[] nameList = str.Split('\n');
        foreach(string g in nameList)
        {
            listName.Add(g);
        }
     
        
    }
    public string RandomName()
    {
        string name;
        int ran = Random.Range(1, listName.Count);
        name = listName[ran];
        listName.Remove(name);
        return name;
    }
    public void GameOver()
    {
        //AdsManager.Instance.showInter();
        isPlaying = false;
      //  Time.timeScale = 0;
        GameOverPanel.SetActive(true);
    }
    public void GoHome()
    {
        AdsManager.Instance.showInter();
        EffectController.Instance.GoMenu();
        Time.timeScale = 1;
        SceneManager.LoadScene(1,LoadSceneMode.Single);    
        GameOverPanel.SetActive(false);
    }
    
}
