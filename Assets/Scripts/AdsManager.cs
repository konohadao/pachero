﻿using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;

public class AdsManager : MonoSingleton<AdsManager> {

     private InterstitialAd interstitial;
     private  BannerView bannerView;
  //   private  RewardBasedVideoAd rewardBasedVideo;
    string appId = "ca-app-pub-3198077663534401~1308064274";
    string idAdMobBanner = "ca-app-pub-3198077663534401/9812212581";
    string idAdMobInterstitial = "ca-app-pub-3198077663534401/1334956761";
    //string idAdModReward = 
    //string appId = "ca-app-pub-3940256099942544~3347511713";
    //string idAdMobBanner = "ca-app-pub-3940256099942544/6300978111";
    //string idAdMobInterstitial = "ca-app-pub-3940256099942544/1033173712";
    public bool isTesting;
    void Awake()
    {
        ShowBanner();
        loadInterAds();
        DontDestroyOnLoad(gameObject);       
    }

    void Start()
    {
      
        MobileAds.Initialize(appId);
        showInter();   
    }
    
    public void ShowBanner()
    {
        AdSize adSize = new AdSize(320, 50);   
        bannerView = new BannerView(idAdMobBanner, adSize, AdPosition.Bottom);
        AdRequest requestBanner = new AdRequest.Builder().Build();
        bannerView.LoadAd(requestBanner);
        bannerView.Show();

    }

    public void loadInterAds()
    {       
        interstitial = new InterstitialAd(idAdMobInterstitial);
        AdRequest request = new AdRequest.Builder().Build();
        interstitial.LoadAd(request);
    }

    public void showInter()
        {
            if (interstitial.IsLoaded() )
            {
                interstitial.Show();
                loadInterAds();
            }
        }
    //public void LoadReward()
    //{
    //    rewardBasedVideo = new RewardBasedVideoAd(idAdModReward);
    //}
}
