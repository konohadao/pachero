﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PacmanBase : MonoBehaviour
{
    protected Color color;
    protected string name;
    protected bool isAive;
    [SerializeField]
    protected float moveSpeed;
    protected Sprite mySprite;
    protected Rigidbody2D rig;

    [SerializeField] protected GameObject textname;
    [SerializeField] GameObject HeathBar;
    [SerializeField] GameObject HeathBarValue;
    protected int Level;
    protected int exp;
    public void SetName(string value)
    {
        if (value.Length >= 9)
        {
            this.name = value.Substring(0, 6) + "...";
        }
        else
            this.name = value;
    }
    public string GetName()
    {
        return this.name;
    }
    public float GetLocalScale()
    {
        return this.exp;
    }
   void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        exp = 1;
       
    }

    void CaculateLevel()
    {
        Level = exp / 10;
    }
    protected void TakeExp(int value)
    {
        this.exp += value;
        CaculateLevel();
        Vector3 localScale = this.transform.localScale;
        if (localScale.x <= 1)
        {
            localScale.x += 0.02f;
            localScale.y += 0.02f;
            textname.GetComponent<TextFollow>().setOffet(0.05f); 
        }
        else
        {
            localScale.x += 0.01f;
            localScale.y += 0.01f;
            textname.GetComponent<TextFollow>().setOffet(0.01f);

        }
        this.transform.localScale = localScale;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if(this.transform.localScale.x > collision.gameObject.transform.localScale.x)
            {
                if (collision.gameObject.name == "Player")
                {
                    IOGameController.Instance.GameOver();
                }
                this.exp += 3;
               // Spawner.Instance.CreateExpOnDead(collision.gameObject.transform.position, this.exp);
                collision.gameObject.transform.position = new Vector3(10000, 10000, 10000);
                collision.gameObject.GetComponent<Bot>().moveSpeed = 0;
               
            }
        }
    }
}
