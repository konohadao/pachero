﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashScene : MonoBehaviour
{
    public GameObject LoadingImage;
    public int sceneIndex;
    public int timeToWait;



    // Update is called once per frame
    void Update()
    {
        LoadingImage.transform.Rotate(new Vector3(0,0,-1) * 200 * Time.deltaTime);
    }
    void Start()
    {
      
        StartCoroutine(LoadAsynchoronously(sceneIndex));
    }

    IEnumerator LoadAsynchoronously(int sceneIndex)
    {
        yield return new WaitForSeconds(timeToWait);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        while (!operation.isDone)
        {
            yield return null;
        }
    }
}
